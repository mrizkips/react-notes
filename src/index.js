import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import * as firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyB86t4Y0nZnuEZDJA_5iq2xDJlTeHnfdzw",
    authDomain: "react-notes-app-7336d.firebaseapp.com",
    databaseURL: "https://react-notes-app-7336d.firebaseio.com",
    projectId: "react-notes-app-7336d",
    storageBucket: "react-notes-app-7336d.appspot.com",
    messagingSenderId: "958513952190",
    appId: "1:958513952190:web:46c357d6e51e66ec55f5ef",
    measurementId: "G-EWP57NHPK7"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

ReactDOM.render(<App />, document.getElementById('root'));

serviceWorker.unregister();
